class vacunas():
    def __init__(self, nombre):
        self.nombre = nombre
        self.lab = None
        self.efectoseg = None
   
    def set_lab(self):
        self.lab = None
       
    def get_lab(self):
        return self.lab
   
    def set_agrega_efecto_secundario(self):
        self.efectoseg = None
       
    def get_efecto_secundario(self):
        return self.efectoseg
   
pfizer = vacunas("pfizer")
pfizer.lab = "Pfizer inc"
pfizer.efectoseg = ["Cansancio","Dolor de cabeza","Dolor muscular","Escalofríos","Fiebre","Náuseas"]

print("\nEsta vacuna es ", pfizer.nombre, "del laboratorio", pfizer.lab , "y sus efectos secundarios son:")
for efecto in pfizer.efectoseg:
    print(efecto)
   
coronavac = vacunas("coronavac")
coronavac.lab = "Sinovavc"
coronavac.efectoseg =    [ "Fiebre","Cansancio","Dolor de cabeza","Dolormuscular","Escalofríos","Diarrea","Dolor en la zona de la inyección"]
print ("\nEsta vacuna es ", coronavac.nombre, "del laboratorio", coronavac.lab ,"y sus efectos secundarios son:")

for efecto in coronavac.efectoseg:
    print(efecto)

cansino = vacunas("cansino")
cansino.lab = "CanSino biologicals Inc"
cansino.efectoseg = [    "Fiebre",
    "Cansancio",
    "Dolor de cabeza",
    "Dolor muscular",
    "Diarrea",
    "Dolor en la zona de la inyección"]
print ("\nEsta vacuna es ", cansino.nombre, "del laboratorio", cansino.lab, "y sus efectos secundarios son:")
for efecto in cansino.efectoseg:
    print(efecto)
